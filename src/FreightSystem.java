import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Finding optimal path for Freight System using A* heuristic Search
 * 
 *
 * Heuristic Analysis.
 * - Logic/Methodology
 * The logic is that goal state will ultimately have no job to complete, 
 * hence heuristic value for a given state is addition of cost of each jobs to complete
 * and the cost of the search state.
 * 
 * - Why is this method admissible.
 * First, this method is admissible because it does not over estimate the heuristic value
 * because essentially each state is compared to the goal state in terms of number of jobs
 * to complete.And that difference cannot be over estimated as its a simple subtraction/addition
 * of cost of jobs to complete compare to no jobs to complete.
 * 
 * - Complexity of heuristic
 * Given n is number of jobs to complete, complexity will be O(n) since it goes through
 * each jobs and add cost of each job to heuristic value.
 * @author Moosuk Pyun
 *
 */
public class FreightSystem {
	ArrayList<String> cityList;
	Map<String, Integer> costMap;
	ArrayList<Job> jobList;
	ArrayList<Edge> edgeList;
	Graph map;
	
	public FreightSystem() {
		this.cityList = new ArrayList<String>();
		this.costMap = new HashMap<String, Integer>();
		this.jobList = new ArrayList<Job>();
		this.edgeList = new ArrayList<Edge>();
	}
	
	/**
	 *  Calls A* Search and retrieve goalState and retrieve its parents.
	 *  And display the path.
	 */
	public void search() {
		// Create Starting search state
		SearchState startState = new SearchState("Sydney", null, jobList, null, 0);
		AStar starSearch = new AStar(startState, map, jobList, jobList.size());
		starSearch.search();
		if (starSearch.checkNoSoln()) {
			System.out.println("No solution");
			return;
		}
		starSearch.setTotalCost();
		String totalCost = Integer.toString(starSearch.getTotalCost());
		String numNode = Integer.toString(starSearch.getNumNode());
		ArrayList<SearchState> path =  starSearch.getPath();
		
		System.out.println(numNode + " nodes expanded");
		System.out.println("cost = " + totalCost);
		Iterator<SearchState> itr = path.iterator();
		while(itr.hasNext()) {
			SearchState currState = itr.next();
			if (!currState.getDes().equals("None")) {
				System.out.println(currState.getDes() + " " + currState.getParent().getLoc() + " to " + currState.getLoc());
			}
		}
					
	}
	
	/**
	 * Reads input from txt file and set map, job list.
	 * @param inputText
	 */
	public void readInput(String inputText) {
		Scanner sc = null;
		try {
			sc = new Scanner(new FileReader(inputText));
			
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] input = line.split(" ");
				if (input[0].equals("Unloading")) {
					String loc = input[2];
					int cost = Integer.parseInt(input[1]);
					cityList.add(loc);
					costMap.put(loc, cost);
				} else if (input[0].equals("Job")) {
					Job job = new Job(input[1], input[2]);
					jobList.add(job);
					
				} else if (input[0].equals("Cost")) {
					Edge edge = new Edge(input[2], input[3], Integer.parseInt(input[1]));
					edgeList.add(edge);
				}
			}
			sc.close();
			
			
			map = new Graph(cityList.size(), cityList, edgeList);
			// Set related costs to jobs and check if there is faulty job 
			Iterator<Job> itrJob = jobList.iterator();
			while (itrJob.hasNext()) {
				Job jobCheck = itrJob.next();
				int cost = map.costEdge(jobCheck.getStart(), jobCheck.getEnd()) + costMap.get(jobCheck.getEnd());
				jobCheck.setCost(cost);
				if (!(map.hasEdge(jobCheck.getStart(), jobCheck.getEnd()))){
					System.out.println("No solution");
					return;
				}
			}
			
		}
		catch (FileNotFoundException e) {}
		finally {
			if (sc != null) sc.close();
		}
	}
	
	/**
	 * Main function
	 * @param args
	 */
	public static void main(String[] args) {
		FreightSystem system = new FreightSystem();
		system.readInput(args[0]);
		system.search();
	}	
}
