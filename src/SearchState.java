import java.util.ArrayList;
import java.util.Iterator;

/**
 * Search State for A* search to go through
 * @author Moosuk Pyun
 *
 */
public class SearchState implements Comparable<SearchState>{
	SearchState parent;
	String location;
	ArrayList<Job> toComplete;
	ArrayList<Job> completed;
	String description;
	int numJobComp;
	int cost;
	int hValue;
	
	public SearchState (String loc, SearchState parentNode, ArrayList<Job> toDo, ArrayList<Job> done, int costAdd) {
		this.location = loc;
		this.parent = parentNode;
		this.cost = costAdd;
		this.toComplete = toDo;
		if (done == null) {
			this.completed = new ArrayList<Job>();
		} else {
			this.completed = done;
		}
		this.numJobComp = completed.size();
		this.hValue = 0;

	}
	
	/**
	 * compareTo method used to override priority queue sorting, sorted in increasing hValue
	 */
	@Override
	public int compareTo(SearchState ss) {
		return Integer.valueOf(hValue).compareTo(Integer.valueOf(ss.getHValue()));
	}
	
	/**
	 * Checks if one search state to the other search state completes a job
	 * and returns that job
	 * @param endLoc
	 * @return toDo, job that gets completed by moving from parent search state to child
	 */
	public Job getMatchToDo(String endLoc) {
		Job toDo = null;
		Iterator<Job> itr = toComplete.iterator();
		while(itr.hasNext()) {
			Job current = itr.next();
			if (current.isJob(location, endLoc)) {
				toDo = current.clone();
			}
		}
		return toDo;
	}
	
	/**
	 * Getter for parent search state
	 * @return parent, parent search state
	 */
	public SearchState getParent() {
		return parent;
	}
	
	/**
	 * Getter for heuristic value
	 * @return hValue
	 */
	public int getHValue(){
		return hValue;
	}
	
	/**
	 * Set description for search state
	 * "None" - if search state is initial node
	 * "Empty" - if movement does not complete a job
	 * "Job" - if movement completes a job
	 * @param str
	 */
	public void setDes(String str) {
		description = str;
	}
	
	/**
	 * Getter for description
	 * @return
	 * * "None" - if search state is initial node
	 * "Empty" - if movement does not complete a job
	 * "Job" - if movement completes a job
	 * @param str
	 */
	public String getDes() {
		return description;
	}
	
	/**
	 * Getter for to complete job list
	 * @return ArrayList<Job> toComplete
	 */
	public ArrayList<Job> getToDo(){
		return toComplete;
	}
	
	/**
	 * Getter for completed job list
	 * @return ArrayList<Job> completed
	 */
	public ArrayList<Job> getCompleted() {
		return completed;
	}
	
	/**
	 * Getter for cost incurred from getting to this search state
	 * @return cost
	 */
	public int getCost() {
		return this.cost;
	}
	
	/**
	 * Return number of jobs completed
	 * @return completed.size()
	 */
	public int numJobCompleted() {
		return this.completed.size();
	}
	
	/**
	 * Getter for current location
	 * @return location
	 */
	public String getLoc() {
		return this.location;
	}
	
	/**
	 * Checks for triangle inequality, hence search states are equal.
	 * if they are redundant by triangle inequality
	 * @param seenState
	 * @return true, if redundant
	 * 		   false, if not redundant
	 */
	public boolean isEqual(SearchState seenState) {
		if (seenState.getLoc().equals(this.location)) {
			if (seenState.numJobCompleted() == this.numJobComp) {
				
				if (seenState.getParent() != null && this.parent.getParent() != null){
					String seenParent = seenState.getParent().getLoc();
					String currParent = this.parent.getParent().getLoc();
					if (seenParent.equals(currParent)) {
						return true;
					}
					
				} 
			}
		}
		return false;
	}
	
	/**
	 * Setter for hValue
	 * @param value
	 */
	public void setH (int value) {
		this.hValue = value;
	}
	
}
