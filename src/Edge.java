
/**
 * Edge class for two city
 * @author Moosuk Pyun
 *
 */
public class Edge {
	String start;
	String end;
	int cost;
	
	public Edge(String from, String to, int costNum) {
		this.start = from;
		this.end = to;
		this.cost = costNum;
	}
	
	/**
	 * Getter for start city
	 * @return starting city
	 */
	public String getStart() {
		return start;
	}
	
	/**
	 * Getter for end city
	 * @return end city
	 */
	public String getEnd() {
		return end;
	}
	
	/**
	 * Getter for cost between two city
	 * @return cost
	 */
	public int getCost() {
		return cost;
	}

}
