/**
 * Job class with start location, end location and related cost of a job
 * @author moosukpyun
 *
 */
public class Job {
	String start;
	String end;
	int cost;
	
	public Job (String from, String to) {
		this.start = from;
		this.end = to;
	}
	
	/**
	 * Check if certain path is a job.
	 * @param from
	 * @param to
	 * @return true, if path have same starting and end location
	 * 		   false, if path doesn't have same starting and end location
	 */
	public boolean isJob(String from, String to) {
		if (from.equals(start) && to.equals(end)) {
			return true;
		}
		return false;
	}
	
	/**
	 * creates clone of job
	 * return clone of job
	 */
	public Job clone() {
		Job clone = new Job(start,end);
		clone.setCost(cost);
		return clone;
	}
	
	/**
	 * Check if two jobs are equal
	 * @param job
	 * @return true, if they have same starting, end location and have same cost
	 * 		   false, if they don't have same starting, end location and have same cost
	 */
	public boolean isEqual(Job job) {
		if (job.getStart().equals(start) && job.getEnd().equals(end) && job.getCost() == cost){
			return true;
		}
		return false;
	}
	
	/**
	 * Getter for start locations
	 * @return start location
	 */
	public String getStart() {
		return start;
	}
	
	/**
	 * Getter for end location
	 * @return end location
	 */
	public String getEnd() {
		return end;
	}
	
	/**
	 * Setter for cost
	 * @param num
	 */
	public void setCost(int num) {
		this.cost = num;
	}
	
	/**
	 * Getter for cost
	 * @return cost
	 */
	public int getCost() {
		return this.cost;
	}

}
