import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Graph class for setting map matrix
 * @author Moosuk Pyun
 *
 */
public class Graph {
	int size;
	Map<String, Integer> locDict;
	int [][] graph;
	
	public Graph (int num, ArrayList<String> list, ArrayList<Edge> edgeList) {
		this.size = num;
		this.locDict = new HashMap<String, Integer>();
		Iterator<String> itr1 = list.iterator();
		int index = 0;
		while(itr1.hasNext()) {
			locDict.put(itr1.next(), index);
			index++;
		}
		
		// Set 0 if not connected
		this.graph = new int[num][num];
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				this.graph[i][j] = 0;
			}
		}
		
		// Set cost between edges
		Iterator<Edge> itr2 = edgeList.iterator();
		while(itr2.hasNext()) {
			Edge edge = itr2.next();
			int index1 = this.locDict.get(edge.getStart());
			int index2 = this.locDict.get(edge.getEnd());
			int pathCost = edge.getCost();
			this.graph[index1][index2] = pathCost;
			this.graph[index2][index1] = pathCost;
			
		}
	}
	
	/**
	 * Getter for list of all linked locations
	 * @param loc
	 * @return ArrayList<String> locations
	 */
	public ArrayList<String> getPotentialLoc (String loc) {
		ArrayList<String> locations = new ArrayList<String>();
		int index = this.locDict.get(loc);
		for (int i = 0; i < this.size; i++) {
			if (this.graph[index][i] > 0) {
				for (Entry<String, Integer> entry : this.locDict.entrySet()) {
					if (entry.getValue() == i) {
						locations.add(entry.getKey());
					}
				}
			}
		}
		return locations;
	}
	
	/**
	 * Check if there exist a linke between two locations
	 * @param city1
	 * @param city2
	 * @return true, if there is an edge
	 * 	       false, if there is no edge
	 */
	public boolean hasEdge (String city1, String city2) {
		int index1 = this.locDict.get(city1);
		int index2 = this.locDict.get(city2);
		if (this.graph[index1][index2] > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Return cost of edge between two location
	 * @param city1
	 * @param city2
	 * @return cost of a edge between two city
	 */
	public int costEdge (String city1, String city2) {
		int index1 = this.locDict.get(city1);
		int index2 = this.locDict.get(city2);
		
		return this.graph[index1][index2];
	}
	
	/**
	 * prints all the connection for debugging purpose
	 * @param city
	 */
	public void printConnection(String city) {
		int index = this.locDict.get(city);
		for (int i = 0; i < this.size; i++) {
			if (this.graph[index][i] > 0) {
				for (Entry<String, Integer> entry : this.locDict.entrySet()) {
					if (entry.getValue() == i) {
						System.out.println(entry.getKey());
					}
				}
			}
		}
	}

}
