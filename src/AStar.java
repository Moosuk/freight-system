import java.util.ArrayList;
import java.util.Stack;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * AStar class
 * @author Moosuk Pyun
 *
 */
public class AStar {
	int numNode;
	int numJob;
	ArrayList<Job> jobList;
	SearchState start;
	Graph map;
	SearchState goalState;
	int totalCost;
	
	public AStar (SearchState initialNode, Graph inputMap, ArrayList<Job> list, int num) {
		this.numNode = 0;
		this.jobList = list;
		this.start = initialNode;
		this.start.setDes("None");
		this.map = inputMap;
		this.numJob = num;
	}
	
	/**
	 * Search through SearchState using A8 search
	 */
	public void search() {
		HeuristicStrategy strat = new StandardHeuristic();
		PriorityQueue<SearchState> open = new PriorityQueue<SearchState>();
		ArrayList<SearchState> close = new ArrayList<SearchState>();
		open.add(start);
		
		while (open.size() > 0) {
			SearchState current = open.poll();
			close.add(current);
			numNode++;
			/** Debugging
			if (current.getParent() != null) {
				System.out.print(Integer.toString(numNode) + " ");
				System.out.println(Integer.toString(current.getHValue()) + " " + current.getParent().getLoc() + " -> " + current.getLoc());

			} else {
				System.out.println(Integer.toString(current.getHValue()) + " " + current.getLoc());
			}
			**/
			if (current.numJobCompleted() == this.numJob) {
				goalState = current;
				break;
			}
			ArrayList<String> possibleLoc = map.getPotentialLoc(current.getLoc());
			Iterator<String> itr1 = possibleLoc.iterator();
			while(itr1.hasNext()) {
				boolean seen = false;
				String loc = itr1.next();
				if (current.getParent() != null && current.getParent().getParent() != null) {
					if (current.getParent().getParent().equals(loc)) {
						continue;
					}
				}
				
				// get clone of complete job and to do job from parent
				ArrayList<Job> toDoList = new ArrayList<Job>();
				Iterator<Job> itrToDo = current.getToDo().iterator();
				while(itrToDo.hasNext()){
					Job oldJob = itrToDo.next();
					Job newJob = oldJob.clone();
					toDoList.add(newJob);
				}
				ArrayList<Job> compList = new ArrayList<Job>();
				Iterator<Job> itrComp = current.getCompleted().iterator();
				while(itrComp.hasNext()) {
					Job oldJob = itrComp.next();
					Job newJob = oldJob.clone();
					compList.add(newJob);
				}
				
				// get job that gets accomplished due to child creation
				Job doneJob = current.getMatchToDo(loc);
				
				String description = "Empty";
				int cost = current.getCost() + map.costEdge(current.getLoc(), loc);
				if (doneJob != null) {
					description = "Job";
					cost += doneJob.getCost();
					cost -= map.costEdge(current.getLoc(), loc);
					compList.add(doneJob);
					Iterator<Job> jobItr = toDoList.iterator();
					while(jobItr.hasNext()) {
						Job compareJob = jobItr.next();
						if (compareJob.isEqual(doneJob)) {
							jobItr.remove();
						}
					}
				}
				
				SearchState childState = new SearchState(loc, current, toDoList, compList, cost);
				childState.setDes(description);
				int hValue = strat.getHeuristic(toDoList, cost);
				childState.setH(hValue);
				Iterator<SearchState> itrSS = close.iterator();
				// Check for redundancy, triangle inequality or going back to same position
				while(itrSS.hasNext()) {
					SearchState SS = itrSS.next();
					if (childState.isEqual(SS)) {
						seen = true;
					} else if (SS.getParent() != null) {
						String seenParent = SS.getParent().getLoc();
						if (seenParent.equals(childState.getParent().getLoc())) {
							int diffCost = childState.getCost() - SS.getCost();
							int costPath = 2 * (map.costEdge(seenParent, childState.getLoc()));
							if (diffCost == costPath) {
								seen = true;

							} 
						}
					}
				}
				if (!seen) {
					open.add(childState);

				}
			}
		}
	}
	
	/**
	 * Checks if there is a solution.
	 * @return true if there is no solution, false if there is a solution
	 */
	public boolean checkNoSoln() {
		if (goalState == null) {
			return true;
		}
		return false;
	}
	
	/**
	 *  Setter for total cost of a path
	 */
	public void setTotalCost() {
		this.totalCost = goalState.getCost();
	}
	
	/**
	 * Getter for total cost of a path
	 * @return totalCost
	 */
	public int getTotalCost() {
		return this.totalCost;
	}
	
	/**
	 * Getter for number of node expanded
	 * @return numNode
	 */
	public int getNumNode() {
		return this.numNode;
	}
	
	/**
	 * Returns path to the goalState by going through its parents till they find the initial node.
	 * Then put into array in order from initial node to goal state
	 * @return ArrayList<SearchSTate> path
	 */
	public ArrayList<SearchState> getPath() {
		ArrayList<SearchState> path = new ArrayList<SearchState>();
		Stack<SearchState> pathStack= new Stack<SearchState>();
		SearchState currNode = goalState;
		while (currNode.getParent() != null) {
			pathStack.push(currNode);
			currNode = currNode.getParent();
		}
		pathStack.push(currNode);
		
		while (!pathStack.empty()) {
			path.add(pathStack.pop());
		}
		
		return path;

	}
	
}
