import java.util.ArrayList;

/**
 * Interface for heuristic search
 * @author Moosuk Pyun
 *
 */
public interface HeuristicStrategy {
	public int getHeuristic(ArrayList<Job> jobs, int cost);
	
}
