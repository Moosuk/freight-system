import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class for setting Heuristic Value.
 * Heuristic Analysis.
 * - Logic/Methodology
 * The logic is that goal state will ultimately have no job to complete, 
 * hence heuristic value for a given state is addition of cost of each jobs to complete
 * and the cost of the search state.
 * 
 * - Why is this method admissible.
 * First, this method is admissible because it does not over estimate the heuristic value
 * because essentially each state is compared to the goal state in terms of number of jobs
 * to complete.And that difference cannot be over estimated as its a simple subtraction/addition
 * of cost of jobs to complete compare to no jobs to complete.
 * 
 * - Complexity of heuristic
 * Given n is number of jobs to complete, complexity will be O(n) since it goes through
 * each jobs and add cost of each job to heuristic value.
 * @author Moosuk Pyun
 *
 */
public class StandardHeuristic implements HeuristicStrategy{

	@Override
	public int getHeuristic(ArrayList<Job> jobs, int cost) {
		// TODO Auto-generated method stub
		int hValue = 0;
		Iterator<Job> itr = jobs.iterator();
		while(itr.hasNext()) {
			Job job = itr.next();
			hValue += job.getCost();
			
		}
		hValue += cost;
		return hValue;
		
	}


}
